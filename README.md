# Shared Switch Sampler Project

This is the project page for a shareable key switch tester started by /u/WillingAlbatross on Reddit.

The main idea of this project is to send some Switch Testers around the world to people who want them. After they're done, they'd then send it on to someone else and so on.
The purpose of this website is to track the progress of the switch testers as they travel around the world and collect some basic information on their passage.

If you want to start your own shared sampler board and want to take part in the optional tracking provided by this project you can fork this repo and host your own version with whatever tweaks you wish.
Or contact me and I'll add your name and board to the dropdown list on this live site.

## Build Setup
The project uses Nuxt with SSR and an express backend with a MySQL db.


```bash
# Copy the enviroment file and update values
$ cp .env.example .env
$ vim .env

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```
