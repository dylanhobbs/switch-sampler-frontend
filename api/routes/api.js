require('dotenv').config()
const { body, validationResult } = require('express-validator');
const rateLimit = require("express-rate-limit");
var express = require('express');
var router = express.Router();

// var mysql = require('mysql')
// var db = mysql.createConnection({
//     host: process.env.DB_HOST,
//     username: process.env.DB_USER,
//     password: process.env.DB_PASS,
//     database: process.env.DB_DB
// })
var mysql = require('mysql')
console.log(process.env.DB_HOST)
var connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_DB
})
connection.connect()

/* GET users listing. */
const refreshLimiter = rateLimit({
    windowMs: 60 * 60 * 1000, 
    max: 60, 
    message: "That's a lot of refreshing, down with this sort of thing"
  });
router.get('/samplers', refreshLimiter, function(req, res, next) {
    let sql = `SELECT Sampler.name, Entry.display_name, Entry.country, Entry.date_recieved, Entry.shipping_cost, Entry.currency, Entry.note FROM Sampler JOIN Entry ON Entry.samplerId = Sampler.id`;
    // let sql = `SELECT Sampler.name FROM Samplers`;
    connection.query(sql, function(err, data, fields) {
        if (err) throw err;
        res.json({
          status: 200,
          data,
          message: "Samplers retrieved"
        })
      })
  });

  const checkLimiter = rateLimit({
    windowMs: 60 * 30 * 1000, 
    max: 20, 
    message: "That's a lot of requests, easy now"
  });
  router.post('/check', [
    body('code').isString().isLength({min: 6, max: 6}),
  ], checkLimiter, function(req, res, next) {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
        }

        // Verify Code
        let sql = 'SELECT * FROM Sampler WHERE code = ?';
        connection.query(sql, [req.body.code], function(err, data, fields){
            if(err) throw err;
            if(!data || !data.length) {
                return res.status(400).json({status: 400, message: 'Code not recognised'});
            }
            return res.status(200).json({
                status: 200,
                message: "Code approved"
            })
        })
  });

  const postEntryLimiter = rateLimit({
    windowMs: 60 * 1 * 1000, 
    max: 5, 
    message: "You're doing that too much, please try again later"
  });
  router.post('/samplers', [
    body('display_name').isString().isLength({min: 2, max: 64}),
    body('country').isLength({min: 2, max: 2}),
    body('date_recieved').isDate('YYYY-MM-DD'),
    body('shipping_cost').isFloat({min: 0}),
    body('currency').isIn(['€', '$', 'A$', '£', 'JP¥', 'CN¥']),
    body('code').isLength({min:6, max: 6}),
    body('notes').optional().isLength({max: 150})
  ], postEntryLimiter, function(req, res, next) {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          return res.status(422).json({ errors: errors.array() });
        }
        // Verify Code
        let sql = 'SELECT * FROM Sampler WHERE code = ?';
        connection.query(sql, [req.body.code], function(err, data, fields){
            if(err) throw err;
            if(!data || !data.length) {
                return res.status(400).json({status: 400, message: 'Unknown code'});
            }
             
            // I'm sure detructuring is a better option here, but it's late 
            let display_name = req.body.display_name;
            let country = req.body.country;
            let date_recieved = req.body.date_recieved;
            let shipping_cost = req.body.shipping_cost;
            let currency = req.body.currency;
            let note = '';
            if(req.body.note){
                note = req.body.note;
            } 

            // Code is good, continue to duplicate check
            let sql = 'SELECT * FROM Entry WHERE display_name = ? AND country = ? AND date_recieved = ? AND samplerId = ?'
            connection.query(sql, [display_name, country, date_recieved, data[0].id], function(err, doop, fields) {
                if (err) throw err;
                console.log('Duplicate Check');
                console.log(doop)
                if(doop.length !== 0) {
                    return res.status(400).json({status: 400, message: 'Duplicate detected'});
                }
                // Doop check good, insert the new entry
                let sql = 'INSERT INTO Entry (display_name, country, date_recieved, shipping_cost, currency, note, samplerId) VALUES (?, ?, ?, ?, ?, ?, ?)';
               connection.query(sql, [display_name, country, date_recieved, shipping_cost, currency, note, data[0].id], function(err, result, fields){
                if (err) throw err;
                return res.status(200).json({
                    status: 200,
                    message: "Entry recorded"
                })
               })
            })
        })
  });
  
  module.exports = router;
  