export const state = () => ({
    keebs: [],
    entries: [],
  })
  
  export const mutations = {
    setKeebs (state, keebs) {
      state.keebs = keebs;
    },

    setEntries (state, entries) {
        state.entries = entries;
    }
  }
